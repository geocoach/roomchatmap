// Constructor
var socketo;
var currentClients = [];
var rooms = [];
var Chat = function(socketIo) {
    socketo = socketIo.sockets;
    setInterval(emitEcho, 60000);
    setInterval(emitCreatorPosition, 5000);

    socketo.on('connection', function(socket){
        console.log('New connection');

        socket.on('client:introduction', function(data){
            onIntroduction(data, socket);
        });
        socket.on('client:message', function(data){
            onNewMessage(data);
        });
        socket.on('client:roomListReq', function() {
            socket.emit('server:roomList', rooms);
            console.log('Room request!');
            console.log('Avalaible rooms: ' + JSON.stringify(rooms));
        });
        socket.on('client:roomCreateReq', function(data){
            rooms.push(data);
            emitRoomList();
            console.log('Created room ' + data.roomName + ' by ' + data.creator);
            console.log('Avalaible rooms' + JSON.stringify(rooms));
        });
        socket.on('client:joinRoom', function(data){
            console.log('User joined: ' + data.user);
            socket.join(data.room.creator);
            emitToRoomUserJoined(data);

        });
        socket.on('client:leaveRoom', function(data){
            socket.leave(data.user);
            emitToRoomUserLeaved(data);
        });
    });
};

var position = {
    latitude: 50.394880,
    longitude: 18.630091
}

function onIntroduction(data, socket){
    currentClients[data.email] = socket;
    console.log("User: " + data.username + " is connected now!");
    socketo.emit('server:newClient', data );
}

function emitToRoomUserJoined(data){
    socketo.to(data.room.creator).emit('server:userJoinedRoom', {
        message: 'New user joined: ' + data.user,
    });
}

function emitToRoomUserLeaved(data){
    socketo.to(data.room.creator).emit('server:userLeavedRoom', {
        message: 'User: ' + data.user + ' leaved this room',
    });
}

function emitCreatorPosition(){
    position = {
        latitude: position.latitude + 0.00005,
        longitude: position.longitude + 0.00005
    };
    socketo.emit('server:position', position);
}

function emitRoomList(){
    socketo.emit('server:roomList', rooms);
}

function emitEcho(){
    socketo.emit('server:echo', {
        message: 'Server echo!'
    });
}

function onNewMessage(data){
    console.log('New message: ' + JSON.stringify(data));
    if(data.room !== undefined){
        socketo.in(data.room).emit('server:newMessage', {
            message: data.message,
            user: data.user
        });
    } else{
        socketo.emit('server:newMessage', {
            message: data.message,
            user: data.user
        });
    }
}

module.exports = Chat;
